'use strict';

var shell = require('shelljs');
var moment = require('moment');

module.exports =  {
	getVersion: function() {
		var version =  shell.exec('git describe --tags',{silent:true}).output.replace(/(\r\n|\n|\r)/gm,'');
		var date_git = shell.exec('git log -1 --format="%cd"',{silent:true}).output.replace(/(\r\n|\n|\r)/gm,'');
		var date = moment(new Date(date_git));
		var ver = `${version} - ${date.utc().format()}`;
		return ver;
	},
	getShortVersion: function() {
		var version =  shell.exec('git describe --abbrev=0 --tags',{silent:true}).output.replace(/(\r\n|\n|\r)/gm,'');
		return version;
	},
	getSvnVersion: function() {
		var version =  shell.exec('git describe --abbrev=0 --tags',{silent:true}).output.replace(/(\r\n|\n|\r|v)/gm,'');
		return version;
	}
};

