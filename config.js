const _ = require('underscore')
var def = {

}

var config = {
  staging: {
    trackingUrl: 'https://tracking.ghostmonitor.info',
    sitesUrl: 'https://sites.ghostmonitor.info/',
    cdnUrl: 'https://gm-tracking-staging.s3.amazonaws.com',
    zipSuffix: '-staging',
    mode: 'staging'
  },
  production: {
    trackingUrl: 'https://tracking.ghostmonitor.com',
    sitesUrl: 'https://sites.ghostmonitor.com/',
    cdnUrl: 'https://cdn.ghostmonitor.com',
    zipSuffix: '',
    mode: 'production',
    logentriesToken: 'bcf2e2dc-caed-46c2-be34-fd04c287622c'
  }

}

module.exports = function (mode) {
  mode = process.env.NODE_ENV || mode || 'production'
  return _.extend(def, config[mode])
}
