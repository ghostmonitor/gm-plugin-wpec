'use strict';
/* jshint undef: true, unused: true, node: true */

//global.Promise = require('bluebird');
//var fs = Promise.promisifyAll(require('fs'));
//var co = require('co');
//var hacssh = require('hacssh');
module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  var config = require('./config')();
  var version = require('./version').getShortVersion();
  var versionOnlyNumbers = version.replace('v', '');
  var svn_version = require('./version').getSvnVersion();
  console.log(config, version, svn_version);
  var dir = {
    app: './app',
    dist: 'dist',
    tmp: '.tmp',
    root: './'
  };

  grunt.initConfig({
    dir: dir,
    pkg: grunt.file.readJSON('package.json'),
    replace: {
      dist: {
        options: {
          variables: {
            trackingUrl:     config.trackingUrl,
            sitesUrl:     config.sitesUrl,
            cdnUrl:     config.cdnUrl,
            version:    version,
            versionOnlyNumbers: versionOnlyNumbers,
            svn_version: svn_version,
            env:        config.mode,
            logentriesToken: config.logentriesToken
          },
          force: true
        },
        files: [{
          expand: true,
          cwd: '<%= dir.tmp %>/ghostmonitor-wp-ecommerce/',
          dest: '<%= dir.tmp %>/ghostmonitor-wp-ecommerce/',
          src: [
            'config.json',
            'wp-ecommerce-ghostmonitor.php',
            'readme.txt'
          ]
        }]
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= dir.dist %>/{,*/}*',
            '<%= dir.tmp %>/{,*/}*'
          ]
        }]
      }
    },
    copy: {
      init: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= dir.app %>',
          dest: '<%= dir.tmp %>/ghostmonitor-wp-ecommerce',
          src: [
            '**',
          ]
        },{
          expand: true,
          dot: true,
          cwd: '<%= dir.root %>',
          dest: '<%= dir.tmp %>/ghostmonitor-wp-ecommerce',
          src: [
            'composer.lock',
            'composer.json'
          ]
        }]
      }
    },
    shell: {
      build: {
        command: [
          'cd <%= dir.tmp %>/ghostmonitor-wp-ecommerce',
          'curl -sSO https://getcomposer.org/download/1.0.3/composer.phar | php',
          'php composer.phar config -g github-oauth.github.com $GITHUB_ACCESS_TOKEN',
          'php composer.phar install',
          'rm -rf composer.phar',
          'rm -rf ' + ['composer.json', 'composer.lock'].join(' '),
          'find . -type f -name \'*.php\' -print | xargs -n 1 php -l',
          'cd ../..',
          'mkdir -p dist',
        ].join('&&')
      },
      zip: {
        command: [
          'cd <%= dir.tmp %>/ghostmonitor-wp-ecommerce/',
          `export PLUGIN_FILENAME="gm-plugin-wpec-${version}${config.zipSuffix}.zip"`,
          'zip -r ../../<%= dir.dist %>/$PLUGIN_FILENAME ./* -x \*.git\*',
        ].join('&&')
      },
      svn: {
        command: [
          'cd <%= dir.tmp %>',
          'svn co http://plugins.svn.wordpress.org/ghostmonitor-wp-ecommerce/ svn',
          'cd svn',
          `mkdir -p tags/"${svn_version}"`,
          'cd ../ghostmonitor-wp-ecommerce',
          `cp -Rp * ../svn/tags/"${svn_version}"`,
          `cp -Rp * ../svn/trunk`,
          'cd ../..',
          'cp -Rp assets <%= dir.tmp %>/svn',
          'cd <%= dir.tmp %>/svn',
          `find . -type d -iname '*.git' | xargs rm -rf`,
          `find . -type d -iname '*.gitignore' | xargs rm -rf`,
          `find . -type d -iname 'installed.json' | xargs rm -rf`,
          'svn add --force * --auto-props --parents --depth infinity -q',
          `svn ci --no-auth-cache --username=$SVN_USERNAME --password=$SVN_PASSWORD -m "AUTOMATED COMMIT, PLUGIN VERSION: ${svn_version}"`,
        ].join('&&')
      }

    }

  });

  grunt.registerTask('build', [
    'clean',
    'copy:init',
    'replace',
    'shell:build'
  ]);

  grunt.registerTask('build-dev', [
    'clean',
    'copy:init',
    'replace',
    'shell:build',
    'shell:zip'
  ]);

  grunt.registerTask('deploy', [
    'shell:zip',
    'shell:svn'
  ]);


};
