<?php
/**
 * GhostMonitor Integration
 *
 * Allows GhostMonitor tracking code to be inserted into store pages.
 *
 * @class        WPEC_Ghostmonitor
 */

defined( 'ABSPATH' ) or die();

require_once 'ghostmonitor_helper/vendor/autoload.php';

class WPEC_Ghostmonitor
{
    private $plugin_config;

    public function __construct()
    {
        if ( ! session_id() ) {
            session_start();
        }

        
        $config_file_path = plugin_dir_path(__FILE__) . '../config.json';
        $config = file_exists($config_file_path) ? json_decode(file_get_contents($config_file_path)) : false;
        $this->plugin_config = $config;

        $this->db_table_name = 'ghostmonitor_data';
        $this->check_database_changes();

        $this->sess         = session_id();
        $this->id           = 'wpec_ghostmonitor';

        // Define user set variables
        $this->ghostmonitor_domain_name = $this->plugin_config && property_exists($this->plugin_config, 'ghostmonitor_domain_name') ? $this->plugin_config->ghostmonitor_domain_name : get_option('ghostmonitor_domain_name');
        $this->ghostmonitor_id          = $this->plugin_config && property_exists($this->plugin_config, 'ghostmonitor_id') ? $this->plugin_config->ghostmonitor_id : get_option('ghostmonitor_id');
        
        $this->gm_helper = new Ghostmonitor\Helper(
            $this->ghostmonitor_id,
            $this->ghostmonitor_domain_name,
            $config ? $config->trackingUrl : false,
            $config ? $config->cdnUrl : false
        );

        $this->gm_helper->setLogentriesToken( (string)  $config->logentriesToken  );
        $this->gm_helper->setLogPath( GHOSTMONITOR_PLUGIN_PATH . 'log.txt' );
        
        // Actions
        add_action('wp_loaded', array($this, 'refill_cart'));
        // Tracking code
        add_action('wp_footer', array($this, 'ghostmonitor_tracking_code'));

        // Version checking
        if ($this->wpec_version_check()) {

            add_action('wp', array($this, 'checkout_page_check'));
            add_action('wpsc_add_item', array($this, 'send_cart_data'));

            add_action('wpsc_remove_item', array($this, 'send_cart_data'));

            add_action('wpsc_edit_item', array($this, 'send_cart_data'));

            // Webhook for setConversion
            add_action('wpsc_transaction_result_report', array($this, 'send_conversion'));

        }
    }

    public function checkout_page_check() {
        if(wpsc_is_checkout()) {
            $this->send_cart_data();

            /**
             * Inject the checkout page reordering js
            */
            if( get_option('ghostmonitor_email_first') ) {
                $this->gm_helper->logDebug( 'ACTION STARTED: Inject the checkout page reordering js' );
                wp_enqueue_script( 'ghostmonitor_optimize_checkout',  plugins_url( '/optimize_checkout.js', __FILE__ ), array('jquery'), '1.1.0', true );
            }
        }
    }
    public function refill_cart()
    {
        // Get cart ID from url
        $gm_cart = isset($_GET['gm_cart']) ? $_GET['gm_cart'] : false;
        
        $this->gm_helper->logDebug( array( 'ACTION STARTED: wp_loaded', 'refill_cart()', 'GET_PARAMS: ', $_GET ) );
        
        // return if it is not set
        if ($gm_cart === false) return false;
        // Get cart data from database by cart ID
        $cart_data = $this->get_gm_data($gm_cart);
        
        $this->gm_helper->logDebug( array( 'CART DATA' => $cart_data ) );
        // If not found then return
        if ($cart_data === false) return false;

        // Require wpsc's cart class
        global $wpsc_cart;
        // Reset the cart
        $wpsc_cart->empty_cart();
        $lastItem = strtolower(end($cart_data['setCartItem']));
        if( $lastItem['name'] === 'tax' ) {
            array_pop($cart_data['setCartItem']);
        }
        // Iterate through each cart item from the database
        // Then restore the cart
        foreach ($cart_data['setCartItem'] as $item) {

            /**
             * set_item( $itemID, $params, $updater)
             * @var $itemID ID of the item
             * @var $params Extra params, like quantity
             * @var $updater if true it set the quantity of the product
             *      else it just adds it to the previous value
             */
            $wpsc_cart->set_item($item['productId'],array(
                'quantity' => $item['qty'],
                'variation_values' => $item['variation_id']
                ),true);
        }

        // Set the discount if session_id exists
        if ( isset($_GET['ghostmonitor_session_id']) ) {
            $validation = $this->gm_helper->validateDiscount($_GET['ghostmonitor_session_id']);
        } else {
            $validation = array('valid' => false);
        }

        /**
         * @todo Add coupon handling. 
         */

        $cart_url = get_option( 'shopping_cart_url');

        unset($_GET['gm_cart']);

        $parameters = array_map(function ($key, $value) {
            return $key . '=' . $value;
        }, array_keys($_GET), array_values($_GET));
        
        $redirect_url = $cart_url . '?' . implode('&', $parameters);
        
        $this->gm_helper->logDebug( 'REDIRECT URL: ' . $redirect_url );
        
        wp_redirect($redirect_url);
        exit;
    }

    public function send_conversion()
    {
        $this->gm_helper->sendConversionData();
    }

    public function send_cart_data()
    {
        $this->gm_helper->logDebug( 'ACTION STARTED: send_cart_data()' );
        global $wpsc_cart, $wpsc_checkout;
        $setCartItem = array();

        // Create session ID
        $gm_session_id = $this->gm_helper->setGhostmonitorSessionId();

        // Check if gm_cart_item_count is set
        // if not set then eet it with current items in cart
        if(empty($_SESSION['gm_cart_item_count']) || $_SESSION['gm_cart_item_count'] != wpsc_cart_item_count()) {
            $_SESSION['gm_cart_item_count'] = wpsc_cart_item_count();
        }

        if ($gm_session_id === false) {
            return false;
        }
        if( !wpsc_have_cart_items() ) {
            return false;
        }
        while (wpsc_have_cart_items()) {
            $cart = wpsc_the_cart_item();
            $item = array();
            $item['session_id'] = $gm_session_id;
            $item['qtyPrice'] = $this->format_money( wpsc_cart_item_price(false));
            $item['price'] = $this->format_money( wpsc_cart_single_item_price(false));
            $item['name'] = $this->escape_for_json(wpsc_cart_item_name());
            $item['qty'] = wpsc_cart_item_quantity();
            $item['productId'] = wpsc_cart_item_product_id();
            $item['imageUrl'] = $this->format_image_url(wpsc_cart_item_image());
            $item['productUrl'] = $this->format_image_url(wpsc_cart_item_url());
            $variation = $wpsc_cart->cart_item->product_variations;
            $item['variation_id'] = ($variation ? $variation : null);
            $setCartItem[] = $item;
        }

        /**
         * Calculate Total value of the cart
         */
        $wpec_tax = new wpec_taxes_controller();

        $total = $wpsc_cart->calculate_subtotal();
        $total += $wpsc_cart->calculate_total_shipping();
        if ( $wpec_tax->wpec_taxes_isenabled() == false && wpsc_have_cart_items() && $wpec_tax->wpec_taxes_isincluded()) {
            $tax = $wpsc_cart->calculate_total_tax();
            $total += $tax;
            $tax = array(
                'session_id' => $gm_session_id,
                'qtyPrice'   => $tax,
                'price'      => $tax,
                'name'       => 'Tax',
                'qty'        => 1,
                'productId'  => '-1',
                'imageUrl'   => get_option( 'home'),
                'productUrl' => get_option( 'home'),
            );
            array_push($setCartItem,$tax);
        } 
        $total -= $wpsc_cart->coupons_amount;

        $return_url = add_query_arg('gm_cart', $this->sess, get_option( 'shopping_cart_url'));
        $setCartData = array(
            'returnUrl'  => $return_url,
            'session_id' => $gm_session_id,
            'value'      => $this->format_money($total),
            'itemCount'  => wpsc_cart_item_count(),
        );

        if (is_user_logged_in()) {
            $user = wp_get_current_user();
            $setCartData['email'] = $user->user_email;
        }

        $ghostmonitorData = array(
            'setCartData' => $setCartData,
            'setCartItem' => $setCartItem,
        );
        if ($this->gm_helper->validateData($ghostmonitorData) === true) {
            $this->set_gm_data($this->sess, $ghostmonitorData);
            $ghostmonitorData['site_id'] = $this->ghostmonitor_id;
            $ghostmonitorData['session_id'] = $gm_session_id;
            $this->gm_helper->sendGhostData($ghostmonitorData, 2);
        }
    }

    public function ghostmonitor_tracking_code()
    {

        if (true !== $this->wpec_version_check()) {
            $this->send_cart_data();
            if ((strpos($_SERVER['REQUEST_URI'], 'checkout/order-received') !== false) ||
                (strpos($_SERVER['REQUEST_URI'], 'order') !== false && strpos($_SERVER['REQUEST_URI'], 'received') !== false)
            ) {
                $this->send_conversion();
            }
        }

        $is_cart_empty = wpsc_cart_item_count() > 0 ? false : true;

        // $additional_lines = (string)$this->get_option('ghostmonitor_init_script_additions');

        echo $this->gm_helper->ghost_init( $is_cart_empty );
    }

    private function set_gm_data( $session_id, $ghostmonitor_data )
    {
        global $wpdb;
        $wpdb->hide_errors();
        $ghostmonitor_data = serialize( $ghostmonitor_data );

        $table_name = $wpdb->prefix . $this->db_table_name;

        $sql = "INSERT INTO `$table_name` (`session_id`, `data`, `time`)
                VALUES (%s, %s, %d)
                ON DUPLICATE KEY UPDATE
                `data` = %s,
                `time` = %d";

        $prep = $wpdb->prepare( $sql, $session_id, $ghostmonitor_data, current_time('timestamp'), $ghostmonitor_data, current_time('timestamp') );
        return $wpdb->query( $prep );
    }

    private function get_gm_data( $session_id )
    {
        global $wpdb;
        $wpdb->hide_errors();

        $table_name = $wpdb->prefix . $this->db_table_name;

        $result = $wpdb->get_var( "SELECT `data` FROM `$table_name` WHERE `session_id` = '$session_id'" );
        if( $result === null ) {
            return false;
        }

        return unserialize( $result );
    }

    private function check_database_changes()
    {
        if( isset( $this->plugin_config->db_version ) )
        {
            return true;
        }

        $this->create_database_table_if_not_exists();
        $this->move_old_sessions();

        $this->plugin_config->db_version = $this->plugin_config->version;

        $config_file_path = plugin_dir_path( __FILE__ ) . '../config.json';
        if( file_exists( $config_file_path ) ) {
            file_put_contents( $config_file_path, json_encode( $this->plugin_config ) );
        }
    }

    private function create_database_table_if_not_exists()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . $this->db_table_name;

        $sql = "CREATE TABLE IF NOT EXISTS $table_name (
            `session_id` varchar(255) NOT NULL PRIMARY KEY,
            `data` text NOT NULL,
            `time` int NOT NULL
        ) ENGINE='InnoDB' CHARSET='utf8'";

        $wpdb->query($sql);
    }

    private function move_old_sessions()
    {
        global $wpdb;

        $sql = "SELECT * FROM $wpdb->options WHERE `option_name` LIKE '_transient_gmcart_%'";

        $gm_sessions = $wpdb->get_results( $sql );

        foreach( $gm_sessions as $session ) {
            $this->set_gm_data( str_replace('_transient_gmcart_', '', $session->option_name ), maybe_unserialize( $session->option_value ) );
            $wpdb->delete( $wpdb->options, array( 'option_name' => $session->option_name ) );
        }
    }

    private function format_money($number)
    {
        return number_format((float)$number, 2, '.', '');
    }

    private function format_description($text)
    {
        $text = str_replace("'", "\'", strip_tags($text));
        $text = str_replace(array("\r", "\n"), "", $text);

        return $text;

    }

    private function format_image_url($url)
    {
        if (stripos($url, 'http') === false) {
            $url = get_site_url() . $url;
        }

        return $url;
    }

    private function escape_for_json($str)
    {
        return str_ireplace("'", "\'", $str);
    }

    private function wpec_version_check($version = '3.9.0')
    {
        if (version_compare(get_option('wpsc_version'), $version, ">=")) {
            return true;
        } else {
            $this->gm_helper->logError(array('WPEC Version error', (get_option('wpsc_version') ? get_option('wpsc_version') : 'wpec version not found') ));
        }
        return false;
    }
}
