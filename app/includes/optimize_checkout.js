jQuery(document).ready(function () {
	var a = [
		'form[class*="checkout"] input[type="text"][name*="email"]',
		'form[action*="checkout"] input[type="text"][name*="email"]',
		'form[class*="checkout"] input[type="text"][title*="email"]',
		'form[action*="checkout"] input[type="text"][title*="email"]',
		'form[action*="checkout"] input[type="email"]',
		'form[class*="checkout"] input[type="email"]'
	];
	var s = a.join(", ");
	var email = jQuery(s)
	if(email.length === 0)  {
		return;
	}
	email = getCommonParent(email)
	move(email)
})
function getCommonParent(email) {
	if(email.parent().find('input').length > 1) {
		return email
	}
	return getCommonParent(email.parent())
}
function move(email) {
	var first = email.parent().find(email.prop('tagName') ).first()
	first = selectFirst(email,first)
	first.before(email);
}
function selectFirst(email,first) {
	if( first.find('input').length === 0) {
		return selectFirst(email,first.next())
	}
	return first
}