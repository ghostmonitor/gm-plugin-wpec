=== Automated Cart Abandonment Campaign for WP eCommerce with Pre-built Emails ===
Contributors: GhostMonitor
Tags: abandon, abandon cart, abandoned cart, bounce rate, cart abandonment, cart abandonment campaign, cart abandonment email, cart abandonment emails, cart recovery, cart recovery campaign, cart recovery email, cart recovery email series, cart recovery emails, conversion optimization, grow, make money from cart recovery, revenue grow, shopping cart abandonment, shopping cart abandonment campaign, shopping cart abandonment email, shopping cart abandonment emails, shopping cart recovery, shopping cart recovery campaign, shopping cart recovery email, shopping cart recovery emails, ecommerce abandon, wp ecommerce cart abandonment, easy digital downloads cart abandonment, wp ecommerce cart abandonment emails, wp ecommerce cart abandonment solution, wp ecommerce cart recovery, wordpress wp ecommerce cart abandonment, wordpress wp ecommerce cart recovery, wordpress wp ecommerce cart recovery emailsecommerce
Requires at least: 3.9
Tested up to: 4.4
Stable tag: @@versionOnlyNumbers
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Send automated emails to cart abandoners and bring them back to complete the checkout. GhostMonitor usually provides 15% revenue growth on autopilot.

== Description ==

This plugin will grow your WP eCommerce revenue 15% by sending automated email campaigns to your cart abandoners and bringing them back to complete their purchase.

How GhostMonitor works behind the scenes to make you money 

You set up GhostMonitor. It works with Shopify, WooCommerce, and WP eCommerce and you'll be able to set it up in under 50 seconds. No marketing or coding skills are needed. Even your mother-in-law could do it. Perhaps.

GhostMonitor capture prospective buyers who abandon their cart. Using our Track & Trigger technology, GhostMonitor captures the visitor's contact details as they're typed on your checkout page. This allows us to send a recovery campaign to those who abandon the checkout, even if they don't submit their details.

GhostMonitor brings them back to complete their checkout. GhostMonitor detects the moment when someone abandons their cart, and immediately sends them an email inviting them to complete their purchase... while your site is still fresh in their mind.

Start growing your online revenue for free
Try [GhostMonitor](https://ghostmonitor.com/?src=wordpress) free for 14 days


= Features =

* 50 seconds setup
* Pre-built Email Campaigns - so you don’t need to write or design emails
* Email Editor - You can change our pre-built emails 
* Recovers prospects' cart with a single click
* Grow your revenue by 15%
* Asynchronous loading that won't affect page load speed
* Automated Cart Recovery - We rebuild the cart for abandoners
* Lost Data Recovery - We capture all data left in checkout page and reload when the prospect returns
* No need to write emails or texts since everything is pre-built
* No marketing or coding skills needed 
* Give discount coupon to cart abandoners 


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the downloaded `.zip` file at `Your Wordpress Dashboard/Plugins/Add new/Upload Plugin`
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Create an account on https://ghostmonitor.com and enter your Site ID in Ghostmonitor

== Frequently Asked Questions ==

= What makes GhostMonitor different from other cart abandonment solutions? =

First, we go about things differently. Unlike other solutions, our Track & Trigger technology detects the moment your visitors abandon their cart. We then send them an email immediately, while others send it 1, 2, 6 or even 24 hours later. Our tests indicate that sending emails right away results in the highest open rates, and recovers more orders. Second, cart abandonment solutions typically charge a monthly fee based on number of visitors or emails sent, and they tie you into a contract. With GhostMonitor, there's only a $10 monthly fee. For more details, see the Pricing page. Third, we have a brilliant team of OCD engineers with type A personalities, whose sole purpose in life is tweaking things to get you better results all the time. Honestly, this one is bordering on having an unfair advantage against your competitors. Some of these people are used to solving problems in ways no-one had ever done before.It's just that they're very "individualistic", so sometimes they're just a little hard to control. But we've learned how to deal with that so far.* *(If you want to know more about what that's like, search for "Cowboys Herding Cats" on youtube.)

= How do you capture cart abandoners' email address? =

It's a secret. If we told you, we'd have to kill you.

OK, maybe that's a little harsh. Alright. We'll tell you.

We have two ways of doing this. The first is using our Track & Trigger technology, which captures their email address as it's typed into the checkout page.

As long as they've completed the email field, we can send them a cart recovery campaign if they abandon the checkout.

The second method is by capturing it if they log-in to the shopping cart.

By the way, we can also capture phone numbers, which your sales team can use to follow up on prospects directly. We're also working to integrate automated text messaging as well!

= What happens after I activate GhostMonitor? Do I need to do anything else? =

You need to thank us for being so ridiculously awesome!

But seriously… once you've activated the GhostMonitor plugin, you're all set. New cart abandoners will begin to receive the 3-email cart recovery campaign. Just sit back and let us do the work of bringing them back for you!

= Can I change the campaign schedule? =

Yes, you can! Go to Campaigns - and edit the mail you want to change. You’ll find the schedule on the top left.

 ust keep in mind… we have an entire team dedicated to constantly testing the emails we send, and we've pre-scheduled your campaigns to provide you with the highest number of recovered orders.

= Can I change the content of the emails? =

Absolutely! Go to Campaigns and click on Edit the email you want to change. It’s easy as 1, 2, 3…

= How can I make sure GhostMonitor drives back the abandoners? =

Do you want the long or the short answer?

Short answer: You can keep track of this stuff in your GhostMonitor dashboard, and with a Google Analytics account.

Long answer: We give you full blown real time statistics of everything that happens, and the amount of money we recovered for you.

We also tag all the links in our recovery emails with UTM parameters, used by Google Analytics.

When the link is clicked and the abandoner returns to your checkout page, the tags can be sent back to Google Analytics and tracked, so you can see how many people GhostMonitor brings back, by looking for the GhostMonitor UTM parameters in your Google Analytics account.

That means you can now get up at 3am to pour over pages and pages of statistics, just like we do!


== Screenshots ==

1. Dashboard
2. Campaigns
3. Email Editor text
4. Email Editor design
5. Ghosts
6. Settings - My account
7. Settings - My site
8. Settings - Billing

== Changelog ==

= 0.4.0 =
* FEATURE: Added an option to optimize checkout page by moving the email field to the top of the form.

= 0.3.2 = 
* FIX: Wrong hook was used for checking if the page is checkout. 

= 0.3.1 = 
* FIX: Ghostmonitor icon is now SVG.

= 0.3.0 = 
* FIX: If wpec was older than 3.9.0 the version check caused error.

= 0.2.0 =
* Added error logging

= 0.1.0 =
* Fist stable version
