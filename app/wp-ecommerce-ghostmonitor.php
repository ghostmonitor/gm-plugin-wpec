<?php
/**
Plugin Name: GhostMonitor WP Ecommerce
Plugin URI: http://www.ghostmonitor.com
Description: Pre-built and Automated Cart Abandonment Campaign for WP Ecommerce
Author: Ghostmonitor INC
Author URI: http://www.ghostmonitor.com
Version: @@versionOnlyNumbers
*/

defined( 'ABSPATH' ) or die();

class WP_eCommerce_Ghostmonitor {
	private $config;
	private $gm_wpec;
	public function __construct()
	{
		// Register hooks
		register_activation_hook( __FILE__, array($this, 'activate_plugin') );
		register_deactivation_hook( __FILE__, array($this, 'deactivate_plugin') );
		define( 'GHOSTMONITOR_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
		if ( is_admin() ){
			// Add ghostmonitor to menu
			add_action( 'admin_menu', array( $this, 'WP_eCommerce_Ghostmonitor' ) );
			// Register settings to the Settings API
			add_action( 'admin_init', array( $this, 'register_settings' ) );
			// Add notice if the ghostmonitor_id is not good
			add_action( 'admin_notices', array( $this, 'display_admin_notice' ) );
			add_action('admin_head', array($this, 'icon_style'));
		}
		// Add settings link for the plugin at the Plugins page 
		add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), array($this, 'add_settings_link') );
		// Read config json
		$this->config = file_exists(plugin_dir_path(__FILE__) . 'config.json') ? json_decode(file_get_contents(plugin_dir_path(__FILE__) . 'config.json')) : false;
		include_once('includes/class-wc-ghostmonitor.php');
		$this->gm_wpec = new WPEC_Ghostmonitor();
	}

	public function activate_plugin()
	{
		$this->send_shop_info();
	}

	public function deactivate_plugin()
	{
		$this->send_shop_info();
	}
	/**
	 * Add ghostmonitor to menu
	 */
	public function WP_eCommerce_Ghostmonitor() 
	{
		add_menu_page( 'Ghostmonitor settings', 'Ghostmonitor', 'manage_options', 'ghostmonitor', array($this,'settings_page'), plugins_url( 'ghostmonitor_ghosty.svg', __FILE__ ), 25);
	}
	public function register_settings() 
	{
		// Define the option name, and validation function
		register_setting( 'ghostmonitor-settings', 'ghostmonitor_id', array($this, 'ghostmonitor_id_validate'));
  		register_setting( 'ghostmonitor-settings', 'ghostmonitor_domain_name', array($this, 'ghostmonitor_domain_validate'));
  		register_setting( 'ghostmonitor-settings', 'ghostmonitor_email_first');

  		// Generate settings group
  		add_settings_section( 'ghostmonitor-settings', 'GhostMonitor Account', array($this,'add_ghostmonitor_settings_option'), 'ghostmonitor');

  		// Add settings fields to the form
  		add_settings_field( 'ghostmonitor_id', 'GhostMonitor Account', array($this,'ghostmonitor_id_option'), 'ghostmonitor', 'ghostmonitor-settings');
  		add_settings_field( 'ghostmonitor_domain_name', 'Your Domain Name', array($this,'ghostmonitor_domain_option'), 'ghostmonitor', 'ghostmonitor-settings');
  		add_settings_field( 'ghostmonitor_email_first', 'Put the email field to the first row', array($this,'ghostmonitor_checkout_option'), 'ghostmonitor', 'ghostmonitor-settings');
	}

	/**
	 * Form generation functions
	 */
	public function add_ghostmonitor_settings_option() {
		echo 'Paste your Site ID here, which can be found in your GhostMonitor <a href="https://app.ghostmonitor.com/#/plugins">account</a>. If you don’t have a GhostMonitor account sign up <a href="https://ghostmonitor.com">free</a> here.';
	}
	public function ghostmonitor_id_option() {
		$ghostmonitor_id = esc_attr(get_option( 'ghostmonitor_id'));
		echo '<input type="text" name="ghostmonitor_id" id="ghostmonitor_id" value="'.$ghostmonitor_id.'"><p class="description">You can find your unique Site ID on the Implementation page inside of your GhostMonitor dashboard.</p>';
	}
	public function ghostmonitor_domain_option() {
		$ghostmonitor_domain_name = esc_attr(get_option( 'ghostmonitor_domain_name'));
		$ghostmonitor_domain_name = esc_attr(get_option( 'ghostmonitor_domain_name'));
		echo '<input type="text" name="ghostmonitor_domain_name" value="'.$ghostmonitor_domain_name.'" disabled><p class="description">This field is automatically generated for you.</p>';
	}
	public function ghostmonitor_checkout_option() {
		$ghostmonitor_id = esc_attr(get_option( 'ghostmonitor_id'));
		echo '<input name="ghostmonitor_email_first" id="ghostmonitor_email_first" type="checkbox" value="1" class="code" ' . checked( 1, get_option( 'ghostmonitor_email_first' ), false ) . ' /> <p class="description">This will generate more identifiable cart abandoners, so you can send more emails.</p>';
	}
	public function icon_style() {
	  echo '<style>
	    #toplevel_page_ghostmonitor .dashicons-before img {
         	width: auto;
		    height: 25px;
    		margin-top: -4px;
	    } 
	  </style>';
	}
	public function settings_page()
	{
		?>
		<div class="wrap">
		<?php settings_errors(); ?>
		<form method="post" action="options.php">
		    <?php settings_fields( 'ghostmonitor-settings' ); ?>
		    <?php do_settings_sections( 'ghostmonitor' ); ?>
		    <?php submit_button(); ?>
		</form>
		</div>
		<?php
	}
	/**
	 * ID validation
	 */
	public function ghostmonitor_id_validate( $input ) {
		if (strlen($input) < 1 ) {
			//add_settings_error('ghostmonitor_id', 'ghostmonitor_id_error', 'Ghostmonitor Account must be set!', 'error');
			return '';
		}
		return $input;
	}
	/**
	 * Domain Validation
	 */
	public function ghostmonitor_domain_validate( $input ) {
		$ghostmonitor_id = get_option('ghostmonitor_id');
		$request = wp_remote_get( "{$this->config->sitesUrl}getdomain?id={$ghostmonitor_id}", array( 'httpversion' => '1.1' ));
		if( $request['response']['code'] !== 200) {
			add_settings_error('ghostmonitor_id', 'ghostmonitor_id_error', 'Ghostmonitor Account ID is not valid!', 'error');
			update_option('ghostmonitor_id','');
			return '';
		}
		return json_decode($request['body'])->domain;
	}
	/**
	 * Add settings link to the plugin on the plugins page
	 */
	public function add_settings_link($links)
	{
		$links[] = '<a href="'. esc_url( get_admin_url(null, 'admin.php?page=ghostmonitor') ) .'">Settings</a>';
		return $links;
	}
	/**
	 * Send info about the wordpress configuration
	 */
	public function send_shop_info()
	{
		require_once 'includes/ghostmonitor_helper/vendor/autoload.php';
		global $wpdb;
		global $wp_version;

		$version = 'v1.0.0';

		//$discount_enabled = get_option( 'woocommerce_enable_coupons' ) === 'yes' ? 'true' : 'false';
		$db_version = (string)$wpdb->get_var("SELECT VERSION()");
		$shop_data  = array(
			'plugin_type' => 'wp_ecommerce',
			'plugin_version' => $this->config ? $this->config->version : $version,
			'engine_version' => get_option('wpsc_version'),
			'wordpress_version' => $wp_version,
			'php_version' => PHP_VERSION,
			'mysql_version' => $db_version,
			'has_curl' => extension_loaded( 'curl' ) ? 'true' : 'false',
			//'is_discount_supported' => $discount_enabled,
		);

		$gm_helper = new Ghostmonitor\Helper(
			get_option('ghostmonitor_id'),
			get_site_url(),
			$this->config ? $this->config->trackingUrl : false,
			$this->config ? $this->config->cdnUrl : false
		);
		$gm_helper->sendShopData( $shop_data );
	}
	/**
	 * Show a notice if the ghostmonitor ID is not ok
	 */
	public function display_admin_notice()
	{
		$gm_config = ($this->config && property_exists($this->config, 'ghostmonitor_id') ? false : true);
		if( $gm_config && ! get_option( 'ghostmonitor_id') && false === strpos( $_SERVER['REQUEST_URI'], 'admin.php?page=ghostmonitor' ) ) :?>
			<div class="update-nag" style="background-color: #ffba00;">
				<?php echo 'Welcome to GhostMonitor! Please make sure that your Site ID is entered correctly in <a href="' . admin_url('admin.php?page=ghostmonitor') . '">Settings</a>.'; ?>
			</div>
		<?php endif;
	}

} 

new WP_eCommerce_Ghostmonitor();